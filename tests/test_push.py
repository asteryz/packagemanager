import ivi
import shutil
import unittest

from io import StringIO
from ivi import shared, version
from pathlib import Path

path = Path(__file__).parent.absolute()
archive_path = path / "ut_pusharchive"
test_path = path / "data" / "ut_pushtest"
output_path = path / "pushoutput"


class PushTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        archive_path.mkdir(parents=True, exist_ok=True)
        test_path.mkdir(parents=True, exist_ok=True)

    @classmethod
    def tearDownClass(cls):
        shutil.rmtree(archive_path)
        shutil.rmtree(test_path)

    def test_push(self):
        ivi.push(path / "data/PushablePackage1.0.0.pack", archive_path)

    def test_pack_push(self):
        with open(path / "data/pack1.json") as packfile:
            package_path = ivi.pack(packfile)

        ivi.push(package_path, archive_path)
