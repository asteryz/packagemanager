import ivi
import unittest
import pathlib
import shutil
import os
import semver

from io import StringIO


path = pathlib.Path(__file__).parent.absolute()
archive_path = path / "ut_archive"
test_path = path / "data" / "ut_test"


class ReadPackfileTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        archive_path.mkdir(parents=True, exist_ok=True)
        test_path.mkdir(parents=True, exist_ok=True)

    @classmethod
    def tearDownClass(cls):
        shutil.rmtree(archive_path)
        shutil.rmtree(test_path)
        os.remove("TestPackage1.0.0.pack")
        os.remove("TestPackage3.0.0.pack")
        os.remove("globpackage1.0.0.pack")
        os.remove("substructurepackage1.0.0.pack")
        os.remove("NotVersionsPackage1.0.0.pack")
        os.remove("VersionsPackage0.0.1.pack")
        os.remove("VersionsPackage0.1.0.pack")
        os.remove("VersionsPackage1.0.0.pack")
        os.remove("VersionsPackage1.0.1-prerelease.pack")
        os.remove("VersionsPackage1.0.1.pack")
        os.remove("VersionsPackage1.0.2-prerelease.pack")
        os.remove("VersionsPackage1.1.0.pack")
        os.remove("VersionsPackage1.1.1.pack")
        os.remove("VersionsPackage2.0.0.pack")

    def test_multipackage(self):
        with open(path / "data/pack1.json") as packfile:
            package_path = ivi.pack(packfile)

        ivi.push(package_path, archive_path)
        with open(path / "data/pack3.json") as packfile:
            package_path = ivi.pack(packfile)

        ivi.push(package_path, archive_path)
        with open(path / "data/multipackagelayout.json") as layoutfile:
            ivi.pull(layoutfile, archive_path)

        one_path = test_path / "multi/one"
        self.assertTrue(os.path.exists(one_path / "test1.data"))
        self.assertTrue(os.path.exists(one_path / "pack.json"))

        three_path = test_path / "multi/three"
        self.assertTrue(os.path.exists(three_path / "test1.data"))
        self.assertTrue(os.path.exists(three_path / "test2.data"))
        self.assertTrue(os.path.exists(three_path / "pack.json"))

    def test_glob(self):
        with open(path / "data/glob_pack.json") as packfile:
            package_path = ivi.pack(packfile)

        ivi.push(package_path, archive_path)
        with open(path / "data/glob_layout.json") as layoutfile:
            ivi.pull(layoutfile, archive_path)

        glob_path = test_path / "glob"
        self.assertTrue(os.path.exists(glob_path / "test1.data"))
        self.assertTrue(os.path.exists(glob_path / "test2.data"))

    def test_substructure(self):
        with open(path / "data" / "substructure_pack.json") as packfile:
            package_path = ivi.pack(packfile)

        ivi.push(package_path, archive_path)
        with open(path / "data" / "substructure_layout.json") as layoutfile:
            ivi.pull(layoutfile, archive_path)

        structure_path = test_path / "substructure/subfolder/structure"
        self.assertTrue(os.path.exists(structure_path / "substructure.data"))

    def test_stringio(self):
        with StringIO(
            """{"name": "TestPackage", "version": "1.0.0", "files": [ "test1.data" ]}"""
        ) as packfile:
            name, version, files = ivi.read_packfile(packfile)
        self.assertEqual(name, "TestPackage")
        self.assertEqual(version, ivi.Version("1.0.0"))
        self.assertEqual(files, ["test1.data"])

    def test_versions_from_archive(self):
        self.maxDiff = None
        packfiles = [
            """{"name": "VersionsPackage", "version": "0.0.1", "files": [ "test1.data" ]}""",
            """{"name": "VersionsPackage", "version": "0.1.0", "files": [ "test1.data" ]}""",
            """{"name": "VersionsPackage", "version": "1.0.0", "files": [ "test1.data" ]}""",
            """{"name": "VersionsPackage", "version": "1.0.1", "files": [ "test1.data" ]}""",
            """{"name": "VersionsPackage", "version": "1.0.1-prerelease", "files": [ "test1.data" ]}""",
            """{"name": "VersionsPackage", "version": "1.0.2-prerelease", "files": [ "test1.data" ]}""",
            """{"name": "VersionsPackage", "version": "1.1.0", "files": [ "test1.data" ]}""",
            """{"name": "VersionsPackage", "version": "1.1.1", "files": [ "test1.data" ]}""",
            """{"name": "VersionsPackage", "version": "2.0.0", "files": [ "test1.data" ]}""",
            """{"name": "NotVersionsPackage", "version": "1.0.0", "files": [ "test2.data" ]}""",
        ]
        files = [ivi.StreamWrapper("layout.json", packfile) for packfile in packfiles]
        for file in files:
            package = ivi.pack(file)
            ivi.push(package, archive_path / "versions")

        expected_versions = sorted(
            [
                ivi.Version(x)
                for x in [
                    "0.0.1",
                    "0.1.0",
                    "1.0.0",
                    "1.0.1",
                    "1.0.1-prerelease",
                    "1.0.2-prerelease",
                    "1.1.0",
                    "1.1.1",
                    "2.0.0",
                ]
            ]
        )
        found_versions = sorted(
            ivi.get_versions_from_archive("VersionsPackage", archive_path / "versions")
        )
        self.assertEqual(found_versions, expected_versions)
        self.assertEqual(
            ivi.find_best_version(ivi.Version("1.0"), found_versions),
            ivi.Version("1.0.1"),
        )
        self.assertEqual(
            ivi.find_best_version(ivi.Version("1.0-prerelease"), found_versions),
            ivi.Version("1.0.2-prerelease"),
        )

