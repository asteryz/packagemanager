import ivi

import unittest


class VersionTest(unittest.TestCase):
    def test_parse(self):
        version = ivi.Version("0.0.4")
        self.assertEqual(version.major, 0)
        self.assertEqual(version.minor, 0)
        self.assertEqual(version.patch, 4)

        version = ivi.Version("10.20.30")
        self.assertEqual(version.major, 10)
        self.assertEqual(version.minor, 20)
        self.assertEqual(version.patch, 30)

        version = ivi.Version("4.19.0-prerelease")
        self.assertEqual(version.major, 4)
        self.assertEqual(version.minor, 19)
        self.assertEqual(version.patch, 0)

        version = ivi.Version("4.19.0-prerelease")
        self.assertEqual(version.prerelease, "prerelease")

        version = ivi.Version("4.19.0+testbuild")
        self.assertEqual(version.build, "testbuild")

        version = ivi.Version("4.19.0-prerelease+testbuild")
        self.assertEqual(version.prerelease, "prerelease")
        self.assertEqual(version.build, "testbuild")

    def test_equality(self):
        self.assertTrue(ivi.Version("1.0.0") < ivi.Version("2.0.0"))
        self.assertTrue(ivi.Version("1.0.0") < ivi.Version("1.1.0"))
        self.assertTrue(ivi.Version("1.0.0") < ivi.Version("1.0.1"))
        self.assertTrue(ivi.Version("1.0.0") == ivi.Version("1.0.0"))

        self.assertFalse(ivi.Version("1.0.0") > ivi.Version("2.0.0"))
        self.assertFalse(ivi.Version("1.0.0") > ivi.Version("1.1.0"))
        self.assertFalse(ivi.Version("1.0.0") > ivi.Version("1.0.1"))
        self.assertFalse(ivi.Version("1.0.0") != ivi.Version("1.0.0"))

        self.assertTrue(ivi.Version("4.19.0") != ivi.Version("4.19.0-prerelease"))
        self.assertTrue(ivi.Version("4.19.0") > ivi.Version("4.19.0-prerelease"))

        self.assertTrue(ivi.Version("4.19.0") == ivi.Version("4.19.0+testbuild"))
        self.assertFalse(ivi.Version("4.19.0") < ivi.Version("4.19.0+testbuild"))
        self.assertFalse(ivi.Version("4.19.0") > ivi.Version("4.19.0+testbuild"))

    def test_find_compatible(self):
        versions = [
            ivi.Version(x)
            for x in ["0.1.0", "0.1.1", "0.1.3", "0.2.0", "1.0.0", "2.0.0"]
        ]
        self.assertEqual(ivi.find_best_version("0.1", versions), ivi.Version("0.1.3"))
        self.assertEqual(ivi.find_best_version("0.2", versions), ivi.Version("0.2.0"))
        self.assertEqual(ivi.find_best_version("0", versions), ivi.Version("0.2.0"))
        self.assertEqual(ivi.find_best_version("1", versions), ivi.Version("1.0.0"))
        self.assertEqual(ivi.find_best_version("", versions), ivi.Version("2.0.0"))

    def test_find_best_prerelease(self):
        versions = [
            ivi.Version(x)
            for x in [
                "0.1.0",
                "1.1.1",
                "1.1.3-prerelease",
                "1.2.0",
                "1.3.0",
                "1.3.0-prerelease",
                "2.0.0",
            ]
        ]
        _ = repr(versions)
        self.assertEqual(ivi.find_best_version("1.1", versions), ivi.Version("1.1.1"))
        self.assertEqual(
            ivi.find_best_version("1.1-prerelease", versions),
            ivi.Version("1.1.3-prerelease"),
        )
        self.assertEqual(ivi.find_best_version("1.2-prerelease", versions), None)
        self.assertEqual(
            ivi.find_best_version("1.3-prerelease", versions),
            ivi.Version("1.3.0-prerelease"),
        )

    def test_find_best_build(self):
        versions = [
            ivi.Version(x)
            for x in [
                "0.1.0+1",
                "1.1.1+2",
                "1.1.3-prerelease+3",
                "1.1.3-prerelease+4",
                "1.2.0+5",
                "1.3.0+6",
                "1.3.0-prerelease+7",
                "1.3.2+8",
                "1.3.2+9" "2.0.0+10",
            ]
        ]
        self.assertEqual(ivi.find_best_version("1.1", versions), ivi.Version("1.1.1"))
        self.assertEqual(ivi.find_best_version("3.0", versions), None)
        self.assertEqual(ivi.find_best_version("1.3.1", versions), None)
        self.assertEqual(ivi.find_best_version("1.3.2", versions), ivi.Version("1.3.2"))

        self.assertEqual(
            ivi.find_best_version("1.1-prerelease", versions),
            ivi.Version("1.1.3-prerelease+3"),
        )
        self.assertEqual(ivi.find_best_version("1.2-prerelease", versions), None)
        self.assertEqual(
            ivi.find_best_version("1.3-prerelease", versions),
            ivi.Version("1.3.0-prerelease"),
        )

