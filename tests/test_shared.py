import ivi
import unittest

from io import StringIO
from ivi import shared, version
from pathlib import Path


class SharedTest(unittest.TestCase):
    def test_package_name(self):
        self.assertEqual(
            shared.build_package_path("TestPackageName", version.Version("1.0.0")),
            Path("TestPackageName/TestPackageName1.0.0.pack"),
        )
        self.assertEqual(
            shared.build_package_path(
                "TestPackageName", version.Version("1.0.0-prerelease")
            ),
            Path("TestPackageName/TestPackageName1.0.0-prerelease.pack"),
        )
        self.assertEqual(
            shared.build_package_path(
                "TestPackageName", version.Version("1.0.0+builddata")
            ),
            Path("TestPackageName/TestPackageName1.0.0+builddata.pack"),
        )

    def test_read_packfile(self):
        with StringIO(
            """{"name": "TestPackage", "version": "1.0.0", "files": [ "test1.data" ]}"""
        ) as packfile:
            name, parsed_version, files = shared.read_packfile(packfile)
            self.assertEqual(name, "TestPackage")
            self.assertEqual(parsed_version, version.Version("1.0.0"))
            self.assertEqual(files, ["test1.data"])
