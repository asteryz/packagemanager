import ivi
import os
import shutil
import unittest

from io import StringIO
from ivi import shared, version
from pathlib import Path

archive_path = Path("ut_pullarchive")
test_path = Path("ut_pulltest")
output_path = Path("pulloutput")


class PullTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        archive_path.mkdir(parents=True, exist_ok=True)
        test_path.mkdir(parents=True, exist_ok=True)

    @classmethod
    def tearDownClass(cls):
        shutil.rmtree(archive_path)
        shutil.rmtree(test_path)
        os.remove("PullPackage1.0.0.pack")
        os.remove("PullPackage3.0.0.pack")

    def test_pull1(self):
        with StringIO(
            """{ "name": "PullPackage", "version": "1.0.0", "files": [ "tests/data/test1.data" ] }"""
        ) as packfile:
            packfile.name = "pack.json"
            package_path = ivi.pack(packfile)

        ivi.push(package_path, archive_path)
        with StringIO(
            """[{"location": "ut_pulltest/packone", "name": "pullpackage", "version": "1.0.0"}]"""
        ) as layoutfile:
            layoutfile.name = "layout.json"
            ivi.pull(layoutfile, archive_path)

        pack_path = test_path / "packone"
        self.assertTrue(Path(pack_path / "tests" / "data" / "test1.data").exists())
        self.assertTrue(Path(pack_path / "pack.json").exists())

    def test_pull3(self):
        with StringIO(
            """{ "name": "PullPackage", "version": "3.0.0", "files": [ "tests/data/test1.data", "tests/data/test2.data" ] }"""
        ) as packfile:
            packfile.name = "pack.json"
            package_path = ivi.pack(packfile)

        ivi.push(package_path, archive_path)
        with StringIO(
            """[{"location": "ut_pulltest/packthree", "name": "pullpackage", "version": "3.0.0"}]"""
        ) as layoutfile:
            layoutfile.name = "layout.json"
            ivi.pull(layoutfile, archive_path)

        pack_path = test_path / "packthree"
        self.assertTrue(Path.exists(pack_path / "tests/data/test1.data"))
        self.assertTrue(Path.exists(pack_path / "tests/data/test2.data"))
        self.assertTrue(Path.exists(pack_path / "pack.json"))
