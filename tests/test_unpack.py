import ivi
import os
import shutil
import unittest

from io import StringIO
from ivi import shared, version
from pathlib import Path

path = Path(__file__).parent.absolute()
archive_path = path / "ut_unpackarchive"
test_path = path / "data" / "ut_unpacktest"
output_path = path / "unpackoutput"


class UnpackTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        archive_path.mkdir(parents=True, exist_ok=True)
        test_path.mkdir(parents=True, exist_ok=True)

    @classmethod
    def tearDownClass(cls):
        shutil.rmtree(archive_path)
        shutil.rmtree(test_path)
        shutil.rmtree(output_path)
        os.remove("TestPackage1.0.0.pack")
        os.remove("TestPackage3.0.0.pack")

    def test_simple(self):
        with open(path / "data/pack1.json") as file:
            package_path = ivi.pack(file)

        ivi.unpack(package_path, output_path)

    def test_unpack3(self):
        with open(path / "data/pack3.json") as file:
            package_path = ivi.pack(file)

        ivi.unpack(package_path, output_path)
        self.assertTrue((output_path / "test1.data").is_file())
        self.assertTrue((output_path / "test2.data").is_file())
        self.assertTrue((output_path / "pack.json").is_file())
