Sometimes you just want data X in location Y. This is a package manager allows you to do just that.
It does not manage dependencies, it does not touch your path, but it does support versioned
packages.


Command line interface:
ivi pack <path to packfile> -> Creates a package based on a packfile
ivi push <path to package> -> Pushes package into the archive
ivi pull <path to layoutfile> -> pulls all packages and places them in the layout specified

ivi unpack <path to package> -> Extract content from a package. 
ivi describe <path to package> -> Output information about the package.
ivi upgrade -> Update ivi to the latest version.


Packfile (pack.json):
pack.json specifies how to create the package. It specifies what is supposed to go in the package,
the package name and the package version.
// Format /////////////////////////////////////////////////////////////////////////////////////////
{
    "name": "TestPackage",
    "version": "1.0",
    "files": [
        "test.data"
    ]
}
///////////////////////////////////////////////////////////////////////////////////////////////////
You can use glob to select multiple files / entire folders folders.

Layoutfile (layout.json):
The layout.json file specifies how the packages should be stored on the disk. These are valid json
files to ensure they can be easily produced and consumed from a variety of environments.

Two formats were considered: 
// Layout A ///////////////////////////////////////////////////////////////////////////////////////
{
    "testpackagefolder": [
        { "name": "testpackage", "version": "1.0" },
        { "name": "otherpackage", "version": "2.0" }
    ]
}
///////////////////////////////////////////////////////////////////////////////////////////////////
Layout A is folder centric which allows you to easily specify two packages that are supposed to go
in a single folder.

// Layout B ///////////////////////////////////////////////////////////////////////////////////////
[
    {
        "location": "testpackagefolder",
        "name": "testpackage",
        "version": "1.0"
    },
    {
        "location": "testpackagefolder",
        "name": "testpackage",
        "version": "1.0"
    }
]
///////////////////////////////////////////////////////////////////////////////////////////////////
Layout B is a package centric format which was deemed more appropriate for the task.