"""
Usage:
    ivi pack <packfile> [options]
    ivi pack -i [options]
    ivi push <package> [<archive>] [options]
    ivi unpack <package> [<root>] [options]
    ivi pull <layoutfile> [<root>] [options]
    ivi pull -i [<root>]
    ivi describe <package>
    ivi upgrade
    ivi version

Options:
    -f --force    Force operation.
    -v --verbose  Turn on verbose output.
    -i --input    Read pack or layout file from standard input
"""
from ivi import cleanup, describe, pack, pull, push, unpack, upgrade, StreamWrapper

from docopt import docopt
from appdirs import AppDirs

import json
import logging
import os
import sys


class IviFormatter(logging.Formatter):
    warning_format = "%(levelname)s: %(message)s"
    info_format = "%(message)s"

    def format(self, record):
        original_format = self._style._fmt
        if record.levelno >= logging.WARNING:
            self._style._fmt = self.warning_format
        else:
            self._style._fmt = self.info_format
        result = logging.Formatter.format(self, record)

        self._style._fmt = original_format

        return result


def setup_logger(verbose):
    handler = logging.StreamHandler(sys.stdout)
    handler.setFormatter(IviFormatter())
    logging.root.addHandler(handler)
    logging.root.setLevel(logging.DEBUG if verbose else logging.INFO)


if __name__ == "__main__":
    # Delete old version if it exists
    cleanup(sys.argv[0])

    args = docopt(__doc__)
    setup_logger(args["--verbose"])

    if args["version"]:
        import json
        with open('pack.json') as packfile:
            json_data = json.loads(packfile.read())
            version = json_data["version"]
            print(version)

    app_dirs = AppDirs("ivi", "MarcSmids")
    config_path = os.path.join(app_dirs.user_config_dir, "config.json")
    if os.path.isfile(config_path):
        with open(config_path) as file:
            config = json.loads(file.read())
    else:
        config = {}

    args_archive = args["<archive>"]
    config_archive = config.get("archive", None)
    default_archive = os.path.join(app_dirs.user_data_dir + "archive")
    archive_path = args_archive or config_archive or default_archive
    logging.debug(f"Using archive at: {archive_path}")

    if args["pack"]:
        if args["--input"]:
            logging.debug(f"Packing packfile from stdin.")
            packfile = StreamWrapper("packfile.json", sys.stdin.read())
            print(packfile.read())
            packfile.seek(0, 0)
            pack(packfile)
        else:
            packfile_path = os.path.normpath(args["<packfile>"])
            logging.debug(f"Packing packfile: {packfile_path}")
            with open(packfile_path) as packfile:
                pack(packfile)
    elif args["push"]:
        package = args["<package>"]
        logging.debug(f"Pushing package {package} to archive at {archive_path}")
        push(package, archive_path)
    elif args["unpack"]:
        package = args["<package>"]
        root = args["<root>"]
        logging.debug(f"Unpacking package {package} at {root}")
        unpack(package, root)
    elif args["pull"]:
        if args["--input"]:
            logging.debug(f"Pulling layoutfile from stdin.")
            pull(sys.stdin, archive_path)
        else:
            layoutfile_path = os.path.normpath(args["<layoutfile>"])
            logging.debug(f"Pulling layoutfile {layoutfile_path}")
            with open(layoutfile_path) as layoutfile:
                pull(layoutfile, archive_path)
    elif args["describe"]:
        package_path = os.path.normpath(args["<package>"])
        logging.debug(f"Describing package {package_path}")
        with open(package_path, 'rb') as package:
            describe(package)
    elif args["upgrade"]:
        upgrade(sys.argv[0], archive_path)
