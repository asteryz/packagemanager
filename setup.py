from setuptools import setup

import json
with open('pack.json') as packfile:
    json_data = json.loads(packfile.read())
    version = json_data["version"]

setup(
    name="ivi",
    version=version,
    description="General purpose package manager",
    url="https://bitbucket.org/asteryz/packagemanager",
    author="Asteryz",
    author_email="asteryz@gmail.com",
    packages=["ivi"],
    install_requires=["appdirs", "docopt", "semver"],
    zip_safe=False,
)

