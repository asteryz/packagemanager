from io import StringIO


class StreamWrapper(StringIO):
    def __init__(self, name, content):
        super().__init__(content)
        self.name = name
