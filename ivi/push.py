from pathlib import Path
from zipfile import ZipFile, ZIP_LZMA

from .shared import logger, read_packfile

import shutil


def push(package_path, archive_path):
    package_path = Path(package_path).resolve()
    archive_path = Path(archive_path).resolve()

    with ZipFile(package_path) as package:
        with package.open("pack.json") as packfile:
            name, _, _ = read_packfile(packfile)

    package_filename = Path(package_path).name
    logger.info(f"Pushing {package_filename} to archive {archive_path}")
    target_path = Path(archive_path) / name
    target_path.mkdir(parents=True, exist_ok=True)
    shutil.copyfile(package_path, target_path / package_filename)
