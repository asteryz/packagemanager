from os import path
from zipfile import ZipFile, ZIP_LZMA

from .shared import logger


def unpack(package_path, location):
    package_path = path.normpath(package_path)
    location = path.normpath(location)

    logger.info(f"Unpacking {str(package_path)} in {str(location)}")
    with ZipFile(package_path) as packagezip:
        packagezip.extractall(path.join(location))
