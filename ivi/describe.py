import json
from zipfile import ZipFile

def describe(package):
    with ZipFile(package) as packagezip:
        with packagezip.open('pack.json') as packfile:
            data = json.loads(packfile.read())
            print(json.dumps(data, indent=4, sort_keys=True))
