from pathlib import Path
from .shared import logger, package_archive_path
from .version import Version, get_versions_from_archive, find_best_version
from .unpack import unpack

import json


def read_layout(layoutfile):
    json_data = json.loads(layoutfile.read())

    for package in json_data:
        package["version"] = Version(package["version"])

    return json_data


def pull(layoutfile, archive_path):
    archive_path = Path(archive_path).resolve()

    layout_dir = Path(layoutfile.name).parent

    logger.info(f"Reading layout file {str(layoutfile.name)}")
    json_data = read_layout(layoutfile)

    for package in json_data:
        available_versions = get_versions_from_archive(package["name"], archive_path)
        best_version = find_best_version(package["version"], available_versions)
        logger.info(f"Fetching {package['name']}{best_version}")
        filepath = package_archive_path(package["name"], best_version, archive_path)
        unpack(filepath, layout_dir / package["location"])
