from glob import glob
from zipfile import ZipFile, ZIP_LZMA
from os import path
from pathlib import Path

import json
import logging
import shutil
import re

from .shared import logger, packaged_filename, read_packfile
from .version import Version, get_versions_from_archive, find_best_version


def gather_files(dir, pattern):
    full = path.join(dir, pattern)
    result = glob(full, recursive=True)
    logger.debug(f"Dir: {dir} Pattern: {pattern} result: [{', '.join(result)}]")
    return result


def pack(packfile):
    packfile_path = path.normpath(packfile.name)
    package_root = path.dirname(packfile_path)

    name, version, patterns = read_packfile(packfile)
    pack_output = packaged_filename(name, version)
    logger.info(f"Packing {name} version: {version}")

    zipfile = ZipFile(pack_output, "w", ZIP_LZMA)
    packfile.seek(0)
    package_description = json.loads(packfile.read())
    files = [
        filename
        for pattern in patterns
        for filename in gather_files(package_root, pattern)
    ]

    for file_path in files:
        file_package_path = path.relpath(file_path, package_root)
        logger.info(f"- {file_package_path}")
        file_package_path
        zipfile.write(file_path, arcname=file_package_path)
    zipfile.writestr("pack.json", json.dumps(package_description))

    logger.info(f"Packaged {len(files)} files into {pack_output}")
    return pack_output

