import os
from io import StringIO
from .pull import pull
from pathlib import Path
import subprocess
import sys

old_exe_name = "ivi-old.exe"

def upgrade(executable_path, archive_path):
    executable_path = Path(executable_path)
    new_name = Path(executable_path).parent / old_exe_name
    os.rename(executable_path, new_name)

    json = f'[{{"location": "{executable_path.parent.as_posix()}", "name": "ivi", "version": ""}}]'
    print(json)
    with StringIO(
        json
    ) as layoutfile:
        layoutfile.name = "layout.json"
        pull(layoutfile, archive_path)

    DETACHED_PROCESS = 0x00000008
    subprocess.Popen([sys.executable, executable_path.as_posix()],
                       creationflags=DETACHED_PROCESS)


def cleanup(executable_path):
    old_path = Path(executable_path).parent / old_exe_name
    print(old_path)
    if old_path.exists():
        print("Deleting")
        os.remove(old_path)
