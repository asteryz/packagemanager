import logging

from pathlib import Path
from .version import Version

import json

logger = logging.getLogger("ivi")


def packaged_filename(package, version):
    return f"{package}{version}.pack"


def build_package_path(package, version):
    filename = packaged_filename(package, version)
    return Path(package, filename)


def package_archive_path(packagename, version, root_path):
    package_filename = build_package_path(packagename, version)
    archive_file = Path(root_path, package_filename)

    if not Path(archive_file).is_file():
        package_filename = packaged_filename(packagename, version)
        archive_file = Path(root_path, package_filename)

    return archive_file


def read_packfile(packfile):
    json_data = json.loads(packfile.read())
    version = Version(json_data["version"])
    return json_data["name"], version, json_data["files"]
