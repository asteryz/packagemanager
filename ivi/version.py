import re
from pathlib import Path


def parse_version(version):
    def chomp_version(version):
        m = re.match(r"^(0|[1-9]\d*)\.?", version)
        return (int(m.group(1)), version[len(m.group(0)) :])

    major = None
    minor = None
    patch = None

    try:
        major, remainder = chomp_version(version)
        minor, remainder = chomp_version(remainder)
        patch, remainder = chomp_version(remainder)
    except AttributeError:
        pass

    return (major, minor, patch)


class Version:
    major = None
    minor = None
    patch = None
    prerelease = None
    build = None

    def parse(self, version_string):
        self.major, self.minor, self.patch = parse_version(version_string)
        try:
            minus_index = version_string.index("-")
            self.prerelease = version_string[minus_index + 1 :]
            plus_index = version_string.index("+")
            self.prerelease = version_string[minus_index + 1 : plus_index]
        except ValueError:
            pass

        try:
            plus_index = version_string.index("+")
            self.build = version_string[plus_index + 1 :]
        except ValueError:
            pass

    def __init__(self, version_string):
        self.parse(version_string)

    def __lt__(self, other):
        if (
            self.major < other.major
            or self.minor < other.minor
            or self.patch < other.patch
        ):
            return True
        elif self.prerelease and other.prerelease:
            return self.prerelease < other.prerelease
        elif self.prerelease:
            return bool(self.prerelease)
        else:
            return False

    def __eq__(self, other):
        return (
            self.major == other.major
            and self.minor == other.minor
            and self.patch == other.patch
            and self.prerelease == other.prerelease
        )

    def __str__(self):
        major = self.major if self.major != None else 0
        minor = self.minor if self.minor != None else 0
        patch = self.patch if self.patch != None else 0
        prerelease = f"-{self.prerelease}" if self.prerelease != None else ""
        build = f"+{self.build}" if self.build != None else ""
        return f"{major}.{minor}.{patch}{prerelease}{build}"

    def __repr__(self):
        return f"Version(major: {self.major}, minor: {self.minor}, patch: {self.patch}, prerelease: {self.prerelease}, build: {self.build})"


def find_best_version(version, versions):
    versions.sort(reverse=True)
    matching_versions = versions

    if isinstance(version, str):
        version = Version(version)

    matching_versions = list(
        filter(lambda x: x.prerelease == version.prerelease, matching_versions)
    )

    if version.major is None:
        return matching_versions[0]

    matching_versions = list(
        filter(lambda x: x.major == version.major, matching_versions)
    )
    if len(matching_versions) < 1:
        return None

    if version.minor is None:
        return matching_versions[0]

    matching_versions = list(
        filter(lambda x: x.minor == version.minor, matching_versions)
    )
    if len(matching_versions) < 1:
        return None

    if version.patch is None:
        return matching_versions[0]

    matching_versions = list(
        filter(lambda x: x.patch == version.patch, matching_versions)
    )
    if len(matching_versions) < 1:
        return None

    return matching_versions[0]


def get_versions_from_archive(packagename, archive_path):
    def get_version_from_path(path):
        ext_len = len(path.suffix)
        return Version(path.name[len(packagename) : -ext_len])

    package_pattern = Path(archive_path) / packagename
    glob_result = package_pattern.glob("**/*")
    test = list(glob_result)
    result = [get_version_from_path(package) for package in test]
    return result
